import json
import hashlib
from math import sqrt

class SD(object): # Copypasted from somewhere on the interwebs
	def __init__(self):
		self.sum, self.sum2, self.n = (0,0,0)
	def sd(self, x):
		self.sum  += x
		self.sum2 += x*x
		self.n    += 1.0
		sum, sum2, n = self.sum, self.sum2, self.n
		self.value = sqrt(sum2/n - sum*sum/n/n)
		return self.value

print "Loading data..."

combinationsfile = open("combinations.json", "r")
weightfile       = open("weights.json", "r")

combinations = json.loads(combinationsfile.read())
weight       = json.loads(      weightfile.read())

print len(combinations), "combinations loaded."

stddev_weight_diffs = [] # Stddev of the daily diff of weights
diff_sums = []           # Sum of the diffs per combination

data = {}

print "Evaluating..."

for combination in combinations:

	id = hashlib.sha256(json.dumps(combination)).hexdigest()
	data[id] = {"a": {}, "b": {}} # Generate the current data element

	stddev_weight_diff = SD() # The stddev of delta weight between A and B

	lastCombination = {
		"a": combination["a"][5],
		"b": combination["b"][5]
	} # The last combination, to be used when calculating book swaps, is the one used on Saturday.

	a_weight = b_weight = 0
	todays_weight = 0
	a_weights_for_delta = []
	max_a_weight = 0
	max_b_weight = 0

	for books in combination["a"]:
		for book in books:
			todays_weight += weight[book]
		a_weights_for_delta.append(todays_weight)
		if todays_weight > max_a_weight:
			max_a_weight = todays_weight
		a_weight += todays_weight
		a_swaps = len(set(books).symmetric_difference(set(lastCombination["a"])))
		lastCombination["a"] = books

	data[id]["a"]["weight"]    = a_weight
	data[id]["a"]["avgweight"] = a_weight / 6
	data[id]["a"]["maxweight"] = max_a_weight
	data[id]["a"]["swaps"]     = a_swaps

	for books in combination["b"]:
		for book in books:
			todays_weight += weight[book]
		if todays_weight > max_b_weight:
			max_b_weight = todays_weight
		# Get the corresponding a_weight (.pop(0)), evaluate the delta, and evaluate the stddev
		stddev_weight_diff.sd(abs(a_weights_for_delta.pop(0)-todays_weight))
		b_swaps = len(set(books).symmetric_difference(set(lastCombination["b"])))
		b_weight += todays_weight
		lastCombination["b"] = books

	data[id]["b"]["weight"]    = b_weight
	data[id]["b"]["avgweight"] = b_weight / 6
	data[id]["b"]["maxweight"] = max_b_weight
	data[id]["b"]["swaps"]     = b_swaps

	data[id]["stddev_weight_diffs"] = stddev_weight_diff.value

print "Evaluated!"
print "Writing to file..."

datafile = open("results.json", "w")
datafile.write(json.dumps(data))

print "Written to results.json!"