import json

gnudatafile      = open("combinations.dat" , "w")
combinationsfile = open("combinations.json", "r")
weightfile       = open("weights.json"     , "r")
formatfile       = open("format.json"      , "r")

combinations = json.loads(combinationsfile.read())
weight       = json.loads(      weightfile.read())
format       = json.loads(      formatfile.read())

bitlength = len(format)
bitrange = range(bitlength)

gnudata = ""
a_weights = []
b_weights = []
diffs = []

previousCombination = {"a": [], "b": []}

for bit in bitrange:
	previousCombination["a"].append(2) # Il valore 2 significa che c'e' sempre un diff. Todo: improve, mettere questo valore a sabato
	previousCombination["b"].append(2)

for combination in combinations:
	diff = 0
	for book in bitrange:
		if combination["a"][book] != previousCombination["a"][book]:
			diff += 1

	diffs.append(diff)

# For some reason unbeknownst to my mortal mind, it turns out the diff for A is the same as the diff for B.
# Todo: switch to book amount rather than bit diff
# Todo: calculate sigma book amount

	currentWeight = 0
	position = 0
	for book in combination["a"]:
		if book:
			currentWeight += weight[format[position]["book"]]
		position += 1
	a_weights.append(currentWeight)

	currentWeight = 0
	position = 0
	for book in combination["b"]:
		if book:
			currentWeight += weight[format[position]["book"]]
		position += 1
	b_weights.append(currentWeight)

	previousCombination = combination

for position in a_weights:
	gnudata += str(a_weights[position]) + " " + str(b_weights[position]) + " " + str(diffs[position]) + "\n"

gnudatafile.write(gnudata)