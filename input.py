import json

schooldays  = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
books       = []    # A list which contains 7 lists, each of which is the list of books for that day.
weights     = {}    # The weight of each book.
uniqueBooks = set() # A unique list of books.

bits = []

print "Separate books with a comma, eg. Book 1, Book 2"
for day in schooldays:
	currentBooks = raw_input("\tBooks on " + day + ": ").replace("\n", "").split(", ")
	for book in currentBooks:
		bits.append({"book": book, "day": day})
	uniqueBooks = uniqueBooks | set(currentBooks)

print "Enter the weight of each book as a whole number, without any unit."
for book in uniqueBooks:
	weights[book] = int(raw_input("\tWeight of " + book + ": "))

formatfile = open("format.json", "w")
weightfile = open("weights.json", "w")

formatfile.write(json.dumps(bits   ))
weightfile.write(json.dumps(weights))