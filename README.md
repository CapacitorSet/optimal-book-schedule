Instructions: run `input.py` and insert the data. Then, run `libri.py` to generate all the possible combinations. Finally, run `toGnuplot.py` to create a gnuplot-compatible data file, called `combinations.dat`.

Todo: move crunching logic to libri.py